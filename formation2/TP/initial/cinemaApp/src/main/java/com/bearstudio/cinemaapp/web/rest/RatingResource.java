package com.bearstudio.cinemaapp.web.rest;

import java.util.List;

import com.bearstudio.cinemaapp.domain.*;
import com.bearstudio.cinemaapp.service.RatingService;

import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class RatingResource {

    private final RatingService ratingService;

    public RatingResource(RatingService ratingService) {
        this.ratingService = ratingService;
    }

    /* Voir la note moyenne d'un film
    -> /api/rating/{movieId} GET
    */

    /* Ajouter une note à un film
    -> /api/rating POST
    */
}