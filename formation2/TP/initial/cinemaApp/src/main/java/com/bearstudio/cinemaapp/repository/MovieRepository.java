package com.bearstudio.cinemaapp.repository;

import java.util.ArrayList;
import java.util.List;

import com.bearstudio.cinemaapp.domain.*;

import org.springframework.stereotype.Repository;

@Repository
public class MovieRepository {

    private MovieDatabase movieDatabase;

    public MovieRepository() {
        this.movieDatabase = new MovieDatabase();
    }

    public void createMovie(Movie movie) {
        this.movieDatabase.load();
        ArrayList allMovies = this.movieDatabase.getMovies();
        if (allMovies != null) {
            movie.setId(allMovies.size() + 1);
            allMovies.add(movie);
            this.movieDatabase.setMovies(allMovies);
        } else {
            ArrayList movies = new ArrayList<>();
            movie.setId(1);
            movies.add(movie);
            this.movieDatabase.setMovies(movies);
        }
        this.movieDatabase.save();
    }

    public Movie getMovie(int id) {
        this.movieDatabase.load();
        ArrayList allMovies = this.movieDatabase.getMovies();
        if (allMovies != null) {
            Movie movieToReturn = null;
            for (Object movie : allMovies) {
                Movie actualMovie = (Movie) movie;
                if (actualMovie.getId() == id) {
                    movieToReturn = actualMovie;
                }
            }
            return movieToReturn;
        }
        return null;
    }

    public void deleteMovie(int id) {
        this.movieDatabase.load();
        ArrayList allMovies = this.movieDatabase.getMovies();
        if (allMovies != null) {
            Movie movieToDelete = null;
            for (Object movie : allMovies) {
                Movie actualMovie = (Movie) movie;
                if (actualMovie.getId() == id) {
                    movieToDelete = actualMovie;
                }
            }
            allMovies.remove(movieToDelete);
            this.movieDatabase.setMovies(allMovies);
        }
        this.movieDatabase.save();
    }

    public List<Movie> getMovies() {
        this.movieDatabase.load();
        ArrayList allMovies = this.movieDatabase.getMovies();
        List<Movie> movies = new ArrayList<>();
        allMovies.forEach(movie -> {
            movies.add((Movie) movie);
        });
        return movies;
    }
}