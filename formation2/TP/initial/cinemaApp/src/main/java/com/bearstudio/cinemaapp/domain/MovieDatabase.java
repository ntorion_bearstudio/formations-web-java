package com.bearstudio.cinemaapp.domain;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

public class MovieDatabase implements Serializable{

    private ArrayList movies;

    public MovieDatabase() {
    }

    public ArrayList getMovies() { return movies; }

    public void setMovies(ArrayList movies) { this.movies = movies; }

    public void load() {
        try {
            File file = new File("/tmp/moviesdatabase.txt");
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file));
            ArrayList movies = (ArrayList)ois.readObject();
            this.movies = movies;
            ois.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void save() {
        try {
            File file = new File("/tmp/moviesdatabase.txt");
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file));
            oos.writeObject(movies);
            oos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}