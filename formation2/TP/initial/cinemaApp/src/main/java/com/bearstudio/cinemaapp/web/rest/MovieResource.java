package com.bearstudio.cinemaapp.web.rest;

import java.util.List;

import com.bearstudio.cinemaapp.domain.*;
import com.bearstudio.cinemaapp.service.MovieService;

import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class MovieResource {

    private final MovieService movieService;

    public MovieResource(MovieService movieService) {
        this.movieService = movieService;
    }

    /* Lister tous les films
    -> /api/movies GET
    */

    /* Retourner un film
    -> /api/movie/{id} GET
    */

    /* Supprimer un film
    -> /api/movie/{id} DELETE
    */

    /* Ajouter un film
    -> /api/movie POST
    */
}