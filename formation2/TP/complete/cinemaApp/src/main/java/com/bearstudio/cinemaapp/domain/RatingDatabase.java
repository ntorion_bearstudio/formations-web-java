package com.bearstudio.cinemaapp.domain;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

public class RatingDatabase implements Serializable{

    private ArrayList ratings;

    public RatingDatabase() {
    }

    public ArrayList getRatings() { return ratings; }

    public void setRatings(ArrayList ratings) { this.ratings = ratings; }

    public void load() {
        try {
            File file = new File("/tmp/ratingsdatabase.txt");
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file));
            ArrayList ratings = (ArrayList)ois.readObject();
            this.ratings = ratings;
            ois.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void save() {
        try {
            File file = new File("/tmp/ratingsdatabase.txt");
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file));
            oos.writeObject(ratings);
            oos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}