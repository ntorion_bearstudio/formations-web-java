package com.bearstudio.cinemaapp.service;

import java.util.List;

import com.bearstudio.cinemaapp.domain.*;
import com.bearstudio.cinemaapp.repository.MovieRepository;

import org.springframework.stereotype.Service;

@Service
public class MovieService {

    private final MovieRepository movieRepository;

    public MovieService(MovieRepository movieRepository) {
        this.movieRepository = movieRepository;
    }

    public void createMovie(Movie movie) {
        this.movieRepository.createMovie(movie);
    }

    public List<Movie> getMovies() {
        return this.movieRepository.getMovies();
    }

    public Movie getMovie(int id) {
        return this.movieRepository.getMovie(id);
    }

    public void deleteMovie(int id) {
        this.movieRepository.deleteMovie(id);
    }
}