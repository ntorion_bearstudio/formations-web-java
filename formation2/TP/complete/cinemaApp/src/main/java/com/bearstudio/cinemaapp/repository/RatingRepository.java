package com.bearstudio.cinemaapp.repository;

import java.util.ArrayList;
import java.util.List;

import com.bearstudio.cinemaapp.domain.*;

import org.springframework.stereotype.Repository;

@Repository
public class RatingRepository {

    private RatingDatabase ratingDatabase;

    public RatingRepository() {
        this.ratingDatabase = new RatingDatabase();
    }

    public void createRating(Rating rating) {
        this.ratingDatabase.load();
        ArrayList allRatings = this.ratingDatabase.getRatings();
        if (allRatings != null) {
            allRatings.add(rating);
            this.ratingDatabase.setRatings(allRatings);
        } else {
            ArrayList ratings = new ArrayList<>();
            ratings.add(rating);
            this.ratingDatabase.setRatings(ratings);
        }
        this.ratingDatabase.save();
    }

    public List<Rating> getRatings() {
        this.ratingDatabase.load();
        ArrayList allRatings = this.ratingDatabase.getRatings();
        List<Rating> ratings = new ArrayList<>();
        allRatings.forEach(rating -> {
            ratings.add((Rating) rating);
        });
        return ratings;
    }
}