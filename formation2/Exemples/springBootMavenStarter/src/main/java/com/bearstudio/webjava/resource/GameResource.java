package com.bearstudio.webjava.resource;

import com.bearstudio.webjava.domain.Game;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class GameResource {

    @GetMapping("/game")
    public Game greeting() {
        return new Game(1L, "FIFA 19", 30L);
    }
}