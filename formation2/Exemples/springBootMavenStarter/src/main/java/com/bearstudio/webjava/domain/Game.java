package com.bearstudio.webjava.domain;

public class Game {

    private final Long id;
    private String name;
    private Long price;

    public Game(Long id, String name, Long price) {
        this.id = id;
        this.name = name;
        this.price = price;
    }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public Long getPrice() { return price; }

    public void setPrice(Long price) { this.price = price; }
}