package com.bearstudio.gamesmarket.domain;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

public class GamesStore implements Serializable{

    private ArrayList games;

    public GamesStore() {
    }

    public ArrayList getGames() { return games; }

    public void setGames(ArrayList games) { this.games = games; }

    public void load() {
        try {
            File file = new File("/tmp/gamesstore.txt");
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file));
            ArrayList games = (ArrayList)ois.readObject();
            this.games = games;
            ois.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void save() {
        try {
            File file = new File("/tmp/gamesstore.txt");
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file));
            oos.writeObject(games);
            oos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}