package com.bearstudio.gamesmarket.service;

import java.util.List;

import com.bearstudio.gamesmarket.domain.*;
import com.bearstudio.gamesmarket.repository.GameRepository;

import org.springframework.stereotype.Service;

@Service
public class GameService {

    private final GameRepository gameRepository;

    public GameService(GameRepository gameRepository) {
        this.gameRepository = gameRepository;
    }

    public void createGame(Game game) {
        this.gameRepository.createGame(game);
    }

    public List<Game> getGames() {
        return this.gameRepository.getGames();
    }
}