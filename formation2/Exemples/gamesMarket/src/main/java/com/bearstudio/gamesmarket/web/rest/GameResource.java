package com.bearstudio.gamesmarket.web.rest;

import java.util.List;

import com.bearstudio.gamesmarket.domain.*;
import com.bearstudio.gamesmarket.service.GameService;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class GameResource {

    private final GameService gameService;

    public GameResource(GameService gameService) {
        this.gameService = gameService;
    }

    @PostMapping("/game")
    public void createGame(@RequestBody Game game) {
        gameService.createGame(game);
    }

    @GetMapping("/games")
    public List<Game> getGames() {
        return gameService.getGames();
    }
}