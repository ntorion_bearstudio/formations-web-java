package com.bearstudio.gamesmarket.repository;

import java.util.ArrayList;
import java.util.List;

import com.bearstudio.gamesmarket.domain.*;

import org.springframework.stereotype.Repository;

@Repository
public class GameRepository {

    private GamesStore gamesStore;

    public GameRepository() {
        this.gamesStore = new GamesStore();
    }

    public void createGame(Game game) {
        this.gamesStore.load();
        ArrayList allGames = this.gamesStore.getGames();
        if (allGames != null) {
            game.setId(allGames.size() + 1);
            allGames.add(game);
            this.gamesStore.setGames(allGames);
        } else {
            ArrayList games = new ArrayList<>();
            game.setId(1);
            games.add(game);
            this.gamesStore.setGames(games);
        }
        this.gamesStore.save();
    }

    public List<Game> getGames() {
        this.gamesStore.load();
        ArrayList allGames = this.gamesStore.getGames();
        List<Game> games = new ArrayList<>();
        allGames.forEach(game -> {
            games.add((Game) game);
        });
        return games;
    }
}