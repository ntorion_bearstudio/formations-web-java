/**
 * View Models used by Spring MVC REST controllers.
 */
package com.bearstudio.cinemaapp.web.rest.vm;
