package com.bearstudio.cinemaapp.repository.extended;

import java.util.List;
import com.bearstudio.cinemaapp.domain.Movie;
import com.bearstudio.cinemaapp.repository.MovieRepository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository extended for the Movie entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MovieRepositoryExtended extends MovieRepository {
  List<Movie> findAllByCinemaCity(String city);

  @Query("select movie from Movie movie where " +
      "(:searchTerm = '' or lower(movie.name) like %:searchTerm%) " +
      "and (:cinema = null or movie.cinema.id = cinema) " +
      "order by movie.releaseDate desc")
  Page<Movie> findAllBySearchTermAndFilters(Pageable pageable, @Param("searchTerm") String searchTerm, @Param("cinema") Long cinema);
}
