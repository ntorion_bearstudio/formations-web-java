package com.bearstudio.cinemaapp.service.extended;

import com.bearstudio.cinemaapp.domain.Cinema;
import com.bearstudio.cinemaapp.repository.CinemaRepository;
import com.bearstudio.cinemaapp.repository.extended.CinemaRepositoryExtended;
import com.bearstudio.cinemaapp.service.CinemaService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * Service Implementation for managing {@link Cinema}.
 */
@Service
@Transactional
public class CinemaServiceExtended extends CinemaService {

    private final Logger log = LoggerFactory.getLogger(MovieServiceExtended.class);

    private final CinemaRepositoryExtended cinemaRepositoryExtended;

    public CinemaServiceExtended(CinemaRepository cinemaRepository, CinemaRepositoryExtended cinemaRepositoryExtended) {
        super(cinemaRepository);
        this.cinemaRepositoryExtended = cinemaRepositoryExtended;
    }

    /**
     * Get all the cinemas.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<Cinema> findAll() {
        log.debug("Request to get all Cinemas");
        return cinemaRepositoryExtended.findAll();
    }
}
