package com.bearstudio.cinemaapp.repository.extended;

import java.util.List;
import com.bearstudio.cinemaapp.domain.Rating;
import com.bearstudio.cinemaapp.repository.RatingRepository;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository extended for the Rating entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RatingRepositoryExtended extends RatingRepository {
  List<Rating> findAllByMovieId(int movieId);
}
