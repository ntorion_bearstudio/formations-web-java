package com.bearstudio.cinemaapp.web.rest.extended;

import com.bearstudio.cinemaapp.domain.Movie;
import com.bearstudio.cinemaapp.domain.dto.MovieDTO;
import com.bearstudio.cinemaapp.service.MovieService;

import com.bearstudio.cinemaapp.service.extended.MovieServiceExtended;
import com.bearstudio.cinemaapp.web.rest.MovieResource;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.bearstudio.cinemaapp.domain.Movie}.
 */
@RestController
@RequestMapping("/api/extended")
public class MovieResourceExtended extends MovieResource {

    private final Logger log = LoggerFactory.getLogger(MovieResourceExtended.class);

    private static final String ENTITY_NAME = "movie";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final MovieServiceExtended movieServiceExtended;

    public MovieResourceExtended(MovieService movieService, MovieServiceExtended movieServiceExtended) {
        super(movieService);
        this.movieServiceExtended = movieServiceExtended;
    }

    /**
     * {@code GET  /movies} : get all the movies.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of movies in body.
     */
    @GetMapping("/movies-search")
    public ResponseEntity<List<MovieDTO>> getAllMoviesBySearchTermAndFilters(
        Pageable pageable,
        @RequestParam MultiValueMap<String, String> queryParams,
        UriComponentsBuilder uriBuilder
    ) {
        log.debug("REST request to get a page of Movies");
        String searchTerm = queryParams.getFirst("searchTerm");
        String cinema = queryParams.getFirst("cinema");

        Long cinemaId = null;
        if (cinema != null && !cinema.equals("null")) {
            cinemaId = Long.parseLong(cinema);
        }
        Page<MovieDTO> page = movieServiceExtended.findAllBySearchTermAndFilters(pageable, searchTerm, cinemaId);

        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /movie/:id} : get the "id" movie.
     *
     * @param id the id of the movie to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the movie, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/movie/{id}")
    public ResponseEntity<MovieDTO> getMovieDTO(@PathVariable Long id) {
        log.debug("REST request to get Movie : {}", id);
        Optional<MovieDTO> movie = movieServiceExtended.findOneById(id);
        return ResponseUtil.wrapOrNotFound(movie);
    }
}
