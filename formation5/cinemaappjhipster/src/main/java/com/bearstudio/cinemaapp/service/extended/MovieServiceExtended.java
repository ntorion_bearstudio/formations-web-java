package com.bearstudio.cinemaapp.service.extended;

import com.bearstudio.cinemaapp.domain.Movie;
import com.bearstudio.cinemaapp.domain.dto.MovieDTO;
import com.bearstudio.cinemaapp.repository.MovieRepository;
import com.bearstudio.cinemaapp.repository.extended.MovieRepositoryExtended;
import com.bearstudio.cinemaapp.service.MovieService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;


/**
 * Service Implementation for managing {@link Movie}.
 */
@Service
@Transactional
public class MovieServiceExtended extends MovieService {

    private final Logger log = LoggerFactory.getLogger(MovieServiceExtended.class);

    private final MovieRepositoryExtended movieRepositoryExtended;

    public MovieServiceExtended(MovieRepository movieRepository, MovieRepositoryExtended movieRepositoryExtended) {
        super(movieRepository);
        this.movieRepositoryExtended = movieRepositoryExtended;
    }

    /**
     * Get all the movies.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<MovieDTO> findAllBySearchTermAndFilters(Pageable pageable, String searchTerm, Long cinemaId) {
        log.debug("Request to get all Movies");
        String searchTermLower = "";
        if (searchTerm != null && !searchTerm.equals("null")) {
            searchTermLower = searchTerm.toLowerCase();
        }
        Page<Movie> movies = movieRepositoryExtended.findAllBySearchTermAndFilters(pageable, searchTermLower, cinemaId);
        Page<MovieDTO> dtoPage = movies.map(m -> new MovieDTO(m));

        return dtoPage;
    }

    /**
     * Get one movie by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<MovieDTO> findOneById(Long id) {
        log.debug("Request to get Movie : {}", id);

        Optional<Movie> movie = movieRepositoryExtended.findById(id);
        Optional<MovieDTO> movieDTO = Optional.empty();
        if (movie.isPresent()) {
            movieDTO = Optional.of(new MovieDTO(movie.get()));
        }
        return movieDTO;
    }
}
