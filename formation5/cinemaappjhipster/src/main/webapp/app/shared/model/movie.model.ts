import { Moment } from 'moment';
import { IRating } from 'app/shared/model/rating.model';
import { ICinema } from 'app/shared/model/cinema.model';

export interface IMovie {
  id?: number;
  name?: string;
  image?: string;
  synopsis?: string;
  releaseDate?: Moment;
  ratings?: IRating[];
  cinema?: ICinema;
}

export const defaultValue: Readonly<IMovie> = {};
