import axios from 'axios';

import { ICinema } from 'app/shared/model/cinema.model';

// Actions

export const getAllCinemas = () => {
  const requestUrl = 'api/extended/cinemas-all';
  return axios.get<ICinema>(requestUrl);
};
