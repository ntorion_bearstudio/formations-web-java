import axios from 'axios';

import { cleanEntity } from 'app/shared/util/entity-utils';

import { IMovie } from 'app/shared/model/movie.model';

const apiUrl = 'api/movies';

// Actions

export const getMovies = (page, size, sort, searchTerm, cinemaId) => {
  const requestUrl = `api/extended/movies-search${
    sort ? `?page=${page}&size=${size}&sort=${sort}&searchTerm=${searchTerm}&cinema=${cinemaId}` : ''
  }`;
  return axios.get<IMovie>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`);
};

export const getMovie = id => {
  const requestUrl = `api/extended/movie/${id}`;
  return axios.get<IMovie>(requestUrl);
};

export const createMovie = entity => axios.post(apiUrl, cleanEntity(entity));

export const updateMovie = entity => axios.put(apiUrl, cleanEntity(entity));

export const deleteMovie = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return axios.delete(requestUrl);
};
