package com.bearstudio.cinemaapp.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * A Movie.
 */
@Entity
@Table(name = "movie")
public class Movie implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @NotNull
    @Column(name = "image", nullable = false)
    private String image;

    @NotNull
    @Column(name = "synopsis", nullable = false)
    private String synopsis;

    @NotNull
    @Column(name = "release_date", nullable = false)
    private LocalDate releaseDate;

    @OneToMany(mappedBy = "movie")
    private Set<Rating> ratings = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties("movies")
    private Cinema cinema;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Movie name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public Movie image(String image) {
        this.image = image;
        return this;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getSynopsis() {
        return synopsis;
    }

    public Movie synopsis(String synopsis) {
        this.synopsis = synopsis;
        return this;
    }

    public void setSynopsis(String synopsis) {
        this.synopsis = synopsis;
    }

    public LocalDate getReleaseDate() {
        return releaseDate;
    }

    public Movie releaseDate(LocalDate releaseDate) {
        this.releaseDate = releaseDate;
        return this;
    }

    public void setReleaseDate(LocalDate releaseDate) {
        this.releaseDate = releaseDate;
    }

    public Set<Rating> getRatings() {
        return ratings;
    }

    public Movie ratings(Set<Rating> ratings) {
        this.ratings = ratings;
        return this;
    }

    public Movie addRatings(Rating rating) {
        this.ratings.add(rating);
        rating.setMovie(this);
        return this;
    }

    public Movie removeRatings(Rating rating) {
        this.ratings.remove(rating);
        rating.setMovie(null);
        return this;
    }

    public void setRatings(Set<Rating> ratings) {
        this.ratings = ratings;
    }

    public Cinema getCinema() {
        return cinema;
    }

    public Movie cinema(Cinema cinema) {
        this.cinema = cinema;
        return this;
    }

    public void setCinema(Cinema cinema) {
        this.cinema = cinema;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Movie)) {
            return false;
        }
        return id != null && id.equals(((Movie) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Movie{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", image='" + getImage() + "'" +
            ", synopsis='" + getSynopsis() + "'" +
            ", releaseDate='" + getReleaseDate() + "'" +
            "}";
    }
}
