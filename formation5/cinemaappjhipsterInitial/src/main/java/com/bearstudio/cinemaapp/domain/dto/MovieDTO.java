package com.bearstudio.cinemaapp.domain.dto;


import com.bearstudio.cinemaapp.domain.Cinema;
import com.bearstudio.cinemaapp.domain.Movie;
import com.bearstudio.cinemaapp.domain.Rating;

import java.time.LocalDate;
import java.util.Iterator;

public class MovieDTO {
    private Long id;

    private String name;

    private String image;

    private String synopsis;

    private LocalDate releaseDate;

    private double globalRating;

    private Cinema cinema;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public MovieDTO image(String image) {
        this.image = image;
        return this;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getSynopsis() {
        return synopsis;
    }

    public void setSynopsis(String synopsis) {
        this.synopsis = synopsis;
    }

    public LocalDate getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(LocalDate releaseDate) {
        this.releaseDate = releaseDate;
    }

    public Cinema getCinema() {
        return cinema;
    }

    public void setCinema(Cinema cinema) {
        this.cinema = cinema;
    }

    public double getGlobalRating() {
        return globalRating;
    }

    public void setGlobalRating(double globalRating) {
        this.globalRating = globalRating;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    public MovieDTO(Movie movie) {
        double globalRating = 0.0;
        if (movie.getRatings().size() > 0) {
            for (Iterator<Rating> it = movie.getRatings().iterator(); it.hasNext();) {
                globalRating += it.next().getRating();
            }
            globalRating /= movie.getRatings().size();
        }

        this.id = movie.getId();
        this.name = movie.getName();
        this.image = movie.getImage();
        this.synopsis = movie.getSynopsis();
        this.releaseDate = movie.getReleaseDate();
        this.globalRating = globalRating;
        this.cinema = movie.getCinema();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MovieDTO)) {
            return false;
        }
        return id != null && id.equals(((MovieDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "MovieDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", image='" + getImage() + "'" +
            ", synopsis='" + getSynopsis() + "'" +
            ", releaseDate='" + getReleaseDate() + "'" +
            "}";
    }
}
