package com.bearstudio.cinemaapp.repository.extended;

import java.util.List;
import com.bearstudio.cinemaapp.domain.Movie;
import com.bearstudio.cinemaapp.repository.MovieRepository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository extended for the Movie entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MovieRepositoryExtended extends MovieRepository {
  List<Movie> findAllByCinemaCity(String city);

  /*

  Ajouter la Query pour faire fonctionner la recherche sur les Movies

  Page<Movie> findAllBySearchTermAndFilters(Pageable pageable, String searchTerm, Long cinema);
  */
}
