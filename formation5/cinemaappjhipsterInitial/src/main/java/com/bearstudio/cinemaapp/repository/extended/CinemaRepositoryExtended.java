package com.bearstudio.cinemaapp.repository.extended;

import java.util.List;

import com.bearstudio.cinemaapp.domain.Cinema;
import com.bearstudio.cinemaapp.repository.CinemaRepository;

import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository extended for the Rating entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CinemaRepositoryExtended extends CinemaRepository {
    List<Cinema> findAll();
}
