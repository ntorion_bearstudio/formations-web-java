package com.bearstudio.cinemaapp.web.rest.extended;

import com.bearstudio.cinemaapp.domain.Cinema;
import com.bearstudio.cinemaapp.service.CinemaService;

import com.bearstudio.cinemaapp.service.extended.CinemaServiceExtended;
import com.bearstudio.cinemaapp.web.rest.CinemaResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * REST controller for managing {@link com.bearstudio.cinemaapp.domain.Movie}.
 */
@RestController
@RequestMapping("/api/extended")
public class CinemaResourceExtended extends CinemaResource {

    private final Logger log = LoggerFactory.getLogger(MovieResourceExtended.class);

    private static final String ENTITY_NAME = "cinema";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CinemaServiceExtended cinemaServiceExtended;

    public CinemaResourceExtended(CinemaService cinemaService, CinemaServiceExtended cinemaServiceExtended) {
        super(cinemaService);
        this.cinemaServiceExtended = cinemaServiceExtended;
    }

    /**
     * {@code GET  /cinemas} : get all the cinemas.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of cinemas in body.
     */
    @GetMapping("/cinemas-all")
    public ResponseEntity<List<Cinema>> getAllCinemas() {
        log.debug("REST request to get all cinemas");
        /* Lister tous les cinémas */
        return null;
    }
}
