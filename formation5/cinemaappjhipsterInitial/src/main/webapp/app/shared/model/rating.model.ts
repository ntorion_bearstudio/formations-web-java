import { IMovie } from 'app/shared/model/movie.model';

export interface IRating {
  id?: number;
  rating?: number;
  movie?: IMovie;
}

export const defaultValue: Readonly<IRating> = {};
