import { IMovie } from 'app/shared/model/movie.model';

export interface ICinema {
  id?: number;
  name?: string;
  city?: string;
  movies?: IMovie[];
}

export const defaultValue: Readonly<ICinema> = {};
