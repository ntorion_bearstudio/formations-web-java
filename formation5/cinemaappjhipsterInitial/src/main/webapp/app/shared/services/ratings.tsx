import axios from 'axios';
import { cleanEntity } from 'app/shared/util/entity-utils';

export const createRating = entity => axios.post('api/ratings', cleanEntity(entity));
