import './movies.scss';

import React from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { TextFormat, getSortState, getPaginationItemsNumber, JhiPagination } from 'react-jhipster';
import SearchField from 'react-search-field';
import StarRatingComponent from 'react-star-rating-component';

// tslint:disable-next-line:no-unused-variable
import { ITEMS_PER_PAGE } from 'app/shared/util/pagination.constants';
import { getMovies } from 'app/shared/services/movies';
import { getAllCinemas } from 'app/shared/services/cinemas';

export interface IMovieProps extends RouteComponentProps<{ url: string }> {}

export class Movies extends React.Component<IMovieProps> {
  state = {
    ...getSortState(this.props.location, ITEMS_PER_PAGE),
    movies: null,
    cinemas: null,
    totalItems: 0,
    searchTerm: null,
    cinemaSelected: null
  };

  componentDidMount() {
    this.getEntities();
  }

  sort = prop => () => {
    this.setState(
      {
        order: this.state.order === 'asc' ? 'desc' : 'asc',
        sort: prop
      },
      () => this.sortEntities()
    );
  };

  sortEntities() {
    this.getEntities();
    this.props.history.push(`${this.props.location.pathname}?page=${this.state.activePage}&sort=${this.state.sort},${this.state.order}`);
  }

  handlePagination = activePage => this.setState({ activePage }, () => this.sortEntities());

  getEntities = async () => {
    const { activePage, itemsPerPage, sort, order, searchTerm, cinemaSelected } = this.state;
    const res = await getMovies(activePage - 1, itemsPerPage, `${sort},${order}`, searchTerm, cinemaSelected);
    const resCinemas = await getAllCinemas();
    if (res && res.data && res.headers && resCinemas && resCinemas.data) {
      this.setState({
        movies: res.data,
        totalItems: res.headers['x-total-count'],
        cinemas: resCinemas.data
      });
    }
  };

  handleChangeSearchTerm = searchTerm => {
    this.setState({ searchTerm });
    this.getEntities();
  };

  handleChangeCinema = cinemaId => {
    this.setState({ cinemaSelected: cinemaId });
    this.getEntities();
  };

  render() {
    const { movies, totalItems, cinemas } = this.state;
    return (
      <div>
        <h2 id="movie-heading">Films</h2>
        <Row>
          <Col xs={12} md={8}>
            <SearchField
              placeholder="Rechercher un film"
              onChange={this.handleChangeSearchTerm}
              onEnter={this.handleChangeSearchTerm}
              onSearchClick={this.handleChangeSearchTerm}
            />
          </Col>
          <Col xs={12} md={4}>
            <select onChange={this.handleChangeCinema} className="form-control">
              {cinemas &&
                cinemas.map(cinema => (
                  <option value={cinema.id}>
                    {cinema.name} ({cinema.city})
                  </option>
                ))}
            </select>
          </Col>
        </Row>

        <Row>
          {movies &&
            movies.map(movie => (
              <Col xs={12} md={6} lg={3}>
                <Link to={`/movie?id=${movie.id}`}>
                  <div className="m-3 d-flex justify-content-center flex-column">
                    <div>
                      <img src={movie.image} height={200} width={150} className="centerImage" />
                    </div>
                    <StarRatingComponent
                      name="rate1"
                      editing={false}
                      emptyStarColor="#FFF"
                      starCount={5}
                      value={movie.globalRating}
                      className="centerStars"
                    />
                    <div className="movieTitleContainer">
                      <span className="movieTitle">{movie.name}</span>
                    </div>
                  </div>
                </Link>
              </Col>
            ))}
        </Row>
        <Row className="justify-content-center">
          <JhiPagination
            items={getPaginationItemsNumber(totalItems, this.state.itemsPerPage)}
            activePage={this.state.activePage}
            onSelect={this.handlePagination}
            maxButtons={5}
          />
        </Row>
      </div>
    );
  }
}

export default Movies;
