import './movie-details.scss';

import React from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import StarRatingComponent from 'react-star-rating-component';
import { getUrlParameter } from 'react-jhipster';

// tslint:disable-next-line:no-unused-variable
import { getMovie } from 'app/shared/services/movies';
import { createRating } from 'app/shared/services/ratings';

export interface IMovieProps extends RouteComponentProps<{ url: string }> {}

export class MovieDetails extends React.Component<IMovieProps> {
  state = {
    movie: null,
    rating: null
  };

  componentDidMount() {
    this.getMovie();
  }

  getMovie = async () => {
    const movieId = getUrlParameter('id', this.props.location.search);
    const res = await getMovie(movieId);
    if (res && res.data) {
      this.setState({
        movie: res.data
      });
    }
  };

  onStarClick = nextValue => {
    this.setState({ rating: nextValue });
  };

  handleNotation = () => {
    const { rating, movie } = this.state;
    createRating({
      rating,
      movie
    });
    this.setState({ rating: null });
    this.getMovie();
  };

  render() {
    const { movie, rating } = this.state;
    return (
      <div>
        {movie && (
          <Row>
            <Col xs={12} md={3}>
              <div className="d-flex justify-content-center flex-column">
                <div>
                  <img src={movie.image} height={380} width={250} className="centerImage" />
                </div>
                <StarRatingComponent
                  name="rate1"
                  editing={false}
                  emptyStarColor="#FFF"
                  starCount={5}
                  value={movie.globalRating}
                  className="centerStars"
                />
              </div>
            </Col>
            <Col xs={12} md={9}>
              <div className="movieDescription d-flex justify-content-between flex-column">
                <div>
                  <h2 className="movieTitle">{movie.name}</h2>
                  <div>{movie.synopsis}</div>
                </div>
                <div>
                  <h3>Ajouter une note au film</h3>
                  <div>
                    <StarRatingComponent name="rate1" emptyStarColor="#FFF" starCount={5} value={rating} onStarClick={this.onStarClick} />
                  </div>
                  <div>
                    <span>Commentaire : </span>
                    <textarea className="form-control" />
                  </div>
                  <div className="mt-3">
                    <Button color="secondary" onClick={this.handleNotation}>
                      Noter
                    </Button>
                  </div>
                </div>
              </div>
            </Col>
          </Row>
        )}
      </div>
    );
  }
}

export default MovieDetails;
