import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { ICinema, defaultValue } from 'app/shared/model/cinema.model';

export const ACTION_TYPES = {
  FETCH_CINEMA_LIST: 'cinema/FETCH_CINEMA_LIST',
  FETCH_CINEMA: 'cinema/FETCH_CINEMA',
  CREATE_CINEMA: 'cinema/CREATE_CINEMA',
  UPDATE_CINEMA: 'cinema/UPDATE_CINEMA',
  DELETE_CINEMA: 'cinema/DELETE_CINEMA',
  RESET: 'cinema/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<ICinema>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type CinemaState = Readonly<typeof initialState>;

// Reducer

export default (state: CinemaState = initialState, action): CinemaState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_CINEMA_LIST):
    case REQUEST(ACTION_TYPES.FETCH_CINEMA):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_CINEMA):
    case REQUEST(ACTION_TYPES.UPDATE_CINEMA):
    case REQUEST(ACTION_TYPES.DELETE_CINEMA):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_CINEMA_LIST):
    case FAILURE(ACTION_TYPES.FETCH_CINEMA):
    case FAILURE(ACTION_TYPES.CREATE_CINEMA):
    case FAILURE(ACTION_TYPES.UPDATE_CINEMA):
    case FAILURE(ACTION_TYPES.DELETE_CINEMA):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_CINEMA_LIST):
      return {
        ...state,
        loading: false,
        totalItems: action.payload.headers['x-total-count'],
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_CINEMA):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_CINEMA):
    case SUCCESS(ACTION_TYPES.UPDATE_CINEMA):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_CINEMA):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/cinemas';

// Actions

export const getEntities: ICrudGetAllAction<ICinema> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_CINEMA_LIST,
    payload: axios.get<ICinema>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<ICinema> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_CINEMA,
    payload: axios.get<ICinema>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<ICinema> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_CINEMA,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<ICinema> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_CINEMA,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<ICinema> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_CINEMA,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
