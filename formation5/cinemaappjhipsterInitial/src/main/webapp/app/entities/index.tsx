import React from 'react';
import { Switch } from 'react-router-dom';

// tslint:disable-next-line:no-unused-variable
import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import Cinema from './cinema';
import Movie from './movie';
import Rating from './rating';
/* jhipster-needle-add-route-import - JHipster will add routes here */

const Routes = ({ match }) => (
  <div>
    <Switch>
      {/* prettier-ignore */}
      <ErrorBoundaryRoute path={`${match.url}/cinema`} component={Cinema} />
      <ErrorBoundaryRoute path={`${match.url}/movie`} component={Movie} />
      <ErrorBoundaryRoute path={`${match.url}/rating`} component={Rating} />
      {/* jhipster-needle-add-route-path - JHipster will add routes here */}
    </Switch>
  </div>
);

export default Routes;
