package com.bearstudio.gamesmarket.service;

import java.util.*;

import com.bearstudio.gamesmarket.domain.*;
import com.bearstudio.gamesmarket.repository.EditorRepository;

import org.springframework.stereotype.Service;

@Service
public class EditorService {

    private final EditorRepository editorRepository;

    public EditorService(EditorRepository editorRepository) {
        this.editorRepository = editorRepository;
    }

    public void saveEditor(Editor editor) {
        this.editorRepository.save(editor);
    }
}