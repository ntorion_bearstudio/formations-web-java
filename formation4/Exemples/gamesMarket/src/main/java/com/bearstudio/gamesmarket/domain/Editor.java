package com.bearstudio.gamesmarket.domain;

import java.io.Serializable;

import java.util.*;
import javax.persistence.*;

@Entity
public class Editor implements Serializable{

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id;

    @Column(name="name")
    private String name;

    public Editor() {}

    public Editor(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public int getId() { return id; }

    public void setId(int id) { this.id = id; }
}