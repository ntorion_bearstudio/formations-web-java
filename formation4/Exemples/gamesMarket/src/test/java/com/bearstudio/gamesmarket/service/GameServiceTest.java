package test.java.com.bearstudio.gamesmarket.service;

import java.util.*;

import com.bearstudio.gamesmarket.GamesMarketApplication;
import com.bearstudio.gamesmarket.domain.*;
import com.bearstudio.gamesmarket.repository.GameRepository;
import com.bearstudio.gamesmarket.service.GameService;

import org.springframework.web.bind.annotation.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import javax.persistence.EntityManager;
import static org.assertj.core.api.Assertions.assertThat;
import org.springframework.transaction.annotation.Transactional;

/**
 * Test class for the GameService.
 *
 * @see GameService
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = GamesMarketApplication.class)
public class GameServiceTest {

  @Autowired
  private GameRepository gameRepository;

  @Autowired
  private GameService gameService;

  @Autowired
  private EntityManager entityManager;

  private Game gameGta;

  private Editor editorRockstar;

  @Before
  public void setup() {
      MockitoAnnotations.initMocks(this);
  }

  @Before
  public void initTests() {
    editorRockstar = new Editor();
    editorRockstar.setName("Rockstar Games");

    entityManager.persist(editorRockstar);
    entityManager.flush();

    gameGta = new Game();
    gameGta.setName("GTA V");
    gameGta.setPrice(20);
    gameGta.setEditor(editorRockstar);
  }

  @Test
  @Transactional
  public void testGameCreationInDB() {
    // given
    entityManager.persist(gameGta);
    entityManager.flush();

    // when
    Game foundGame = gameService.getGameByName(gameGta.getName());

    // then
    assertThat(foundGame.getName())
      .isEqualTo(gameGta.getName());
  }
}