package test.java.com.bearstudio.gamesmarket.service;

import java.util.*;

import com.bearstudio.gamesmarket.GamesMarketApplication;
import com.bearstudio.gamesmarket.domain.*;
import com.bearstudio.gamesmarket.repository.GameRepository;
import com.bearstudio.gamesmarket.service.GameService;

import org.springframework.web.bind.annotation.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import javax.persistence.EntityManager;
import static org.assertj.core.api.Assertions.assertThat;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.mockito.Mockito;

/**
 * Test class for the GameService.
 *
 * @see GameService
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = GamesMarketApplication.class)
public class GameServiceTest {

  @MockBean
  private GameRepository gameRepository;

  @Autowired
  private GameService gameService;

  @Autowired
  private EntityManager entityManager;

  private List<Game> games;

  private Editor editorRockstar;

  @Before
  public void setup() {
    MockitoAnnotations.initMocks(this);
  }

  @Before
  public void initTests() {
    games = new ArrayList();

    editorRockstar = new Editor();
    editorRockstar.setName("Rockstar Games");

    Game gameGta = new Game();
    gameGta.setName("GTA V");
    gameGta.setPrice(20);
    gameGta.setEditor(editorRockstar);

    Game gameRed = new Game();
    gameRed.setName("Red Dead Redemption II");
    gameRed.setPrice(60);
    gameRed.setEditor(editorRockstar);

    games.add(gameGta);
    games.add(gameRed);

    Mockito.when(gameRepository.findByName(gameGta.getName()))
      .thenReturn(gameGta);

    Mockito.when(gameRepository.findAll())
      .thenReturn(games);
  }

  @Test
  public void testGameFindByName() {
    // when
    Game foundGame = gameService.getGameByName("GTA V");

    // then
    assertThat(foundGame.getName())
      .isEqualTo("GTA V");
  }

  @Test
  public void testGetGames() {
    // when
    List<Game> allGames = gameService.getGames();

    // then
    assertThat(allGames.size())
      .isEqualTo(2);
  }
}