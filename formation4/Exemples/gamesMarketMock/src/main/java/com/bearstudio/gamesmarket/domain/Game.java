package com.bearstudio.gamesmarket.domain;

import java.io.Serializable;

import javax.persistence.*;

@Entity
public class Game implements Serializable{

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id;

    @Column(name="name")
    private String name;

    @Column(name="price")
    private int price;

    @ManyToOne
    private Editor editor;

    public Game() {}

    public Game(int id, String name, int price, Editor editor) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.editor = editor;
    }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public int getPrice() { return price; }

    public void setPrice(int price) { this.price = price; }

    public Editor getEditor() { return editor; }

    public void setEditor(Editor editor) { this.editor = editor; }

    public int getId() { return id; }

    public void setId(int id) { this.id = id; }
}