package com.bearstudio.cinemaapp.web.rest;

import java.util.*;

import com.bearstudio.cinemaapp.domain.*;
import com.bearstudio.cinemaapp.service.MovieService;

import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class MovieResource {

    private final MovieService movieService;

    public MovieResource(MovieService movieService) {
        this.movieService = movieService;
    }

    @GetMapping("/movies")
    public List<Movie> getMovies() {
        return movieService.getMovies();
    }

    @GetMapping("/movie/{id}")
    public Movie getMovie(@PathVariable int id) {
        Optional<Movie> movie = movieService.getMovie(id);
        if (movie.isPresent()) {
            return movie.get();
        }
        return null;
    }

    @DeleteMapping("/movie/{id}")
    public void deleteMovie(@PathVariable int id) {
        movieService.deleteMovie(id);
    }

    @PostMapping("/movie")
    public void createMovie(@RequestBody Movie movie) {
        movieService.createMovie(movie);
    }

    @PutMapping("/movie")
    public void updateMovie(@RequestBody Movie movie) {
        movieService.updateMovie(movie);
    }

    @GetMapping("/movies/city")
    public List<Movie> getMoviesOnCity(@RequestParam("city") String city) {
        return movieService.getMoviesForCity(city);
    }
}