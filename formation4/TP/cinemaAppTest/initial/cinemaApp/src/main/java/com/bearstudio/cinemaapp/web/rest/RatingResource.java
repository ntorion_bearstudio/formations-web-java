package com.bearstudio.cinemaapp.web.rest;

import java.util.List;

import com.bearstudio.cinemaapp.domain.*;
import com.bearstudio.cinemaapp.service.RatingService;

import org.springframework.web.bind.annotation.*;
import com.bearstudio.cinemaapp.exceptions.*;

@RestController
@RequestMapping("/api")
public class RatingResource {

    private final RatingService ratingService;

    public RatingResource(RatingService ratingService) {
        this.ratingService = ratingService;
    }

    @GetMapping("/rating/{movieId}")
    public double getMovieRating(@PathVariable("movieId") int movieId) {
      try {
        return ratingService.getRatingForMovie(movieId);
      } catch (EmptyRatingException e) {
        e.printStackTrace();
      }
      return 0.0;
    }

    @PostMapping("/rating")
    public void addRatingForMovie(@RequestBody Rating rating) {
        ratingService.addRating(rating);
    }
}