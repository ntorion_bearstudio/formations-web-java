package com.bearstudio.cinemaapp.service;

import java.util.*;
import java.util.stream.*;

import com.bearstudio.cinemaapp.domain.*;
import com.bearstudio.cinemaapp.repository.*;
import com.bearstudio.cinemaapp.exceptions.*;

import org.springframework.stereotype.Service;

@Service
public class RatingService {

    private final RatingRepository ratingRepository;

    public RatingService(RatingRepository ratingRepository) {
        this.ratingRepository = ratingRepository;
    }

    public void addRating(Rating rating) {
        this.ratingRepository.save(rating);
    }

    public double getRatingForMovie(int movieId) throws EmptyRatingException {
        List<Rating> allRatingsForMovie = this.ratingRepository.findAllByMovieId(movieId);

        if(allRatingsForMovie.contains(null)) {
          throw new EmptyRatingException("Empty");
        }

        double rating = 0.0;
        int ratingsSize = 0;

        for (int i = 0; i < allRatingsForMovie.size(); i++) {
          if (allRatingsForMovie.get(i) != null) {
            ratingsSize++;
            rating += allRatingsForMovie.get(i).getRating();
          }
        }
        rating /= ratingsSize;

        return rating;
    }
}