package com.bearstudio.cinemaapp.exceptions;

import java.lang.*;

public class EmptyRatingException extends Exception {
  public EmptyRatingException(String message) {
    super(message);
  }
}