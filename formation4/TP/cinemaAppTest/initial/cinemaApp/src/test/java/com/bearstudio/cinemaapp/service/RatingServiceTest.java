package test.java.com.bearstudio.cinemaapp.service;

import java.beans.Transient;
import java.util.*;

import org.springframework.web.bind.annotation.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.bearstudio.cinemaapp.CinemaApplication;
import com.bearstudio.cinemaapp.domain.Cinema;
import com.bearstudio.cinemaapp.domain.Movie;
import com.bearstudio.cinemaapp.domain.Rating;
import com.bearstudio.cinemaapp.repository.RatingRepository;
import com.bearstudio.cinemaapp.service.RatingService;
import com.bearstudio.cinemaapp.exceptions.*;

import static org.assertj.core.api.Assertions.assertThat;
import org.springframework.transaction.annotation.Transactional;
import org.mockito.*;
import static org.hamcrest.MatcherAssert.*; 
import static org.hamcrest.Matchers.*;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDate;

/**
 * Test class for the RatingService.
 *
 * @see RatingService
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = CinemaApplication.class)
public class RatingServiceTest {

  /* Déclarations des objets et mocks nécessaires */

  @MockBean
  private RatingRepository ratingRepository;

  @Autowired
  private RatingService ratingService;

  @Before
  public void setup() {
    MockitoAnnotations.initMocks(this);
  }

  @Before
  public void initTests() {
    /* Ajouter des données film + notes */
    
    Cinema cinema = new Cinema(1, "Gaumont Caen", "Caen");

    Movie movie = new Movie(1, "Titanic", LocalDate.parse("1997-10-10"), "synopsis", cinema);
    Movie movie2 = new Movie(2, "Avengers", LocalDate.parse("2010-10-10"), "synopsis", cinema);

    Rating rating1 = new Rating(movie, 9);
    Rating rating2 = new Rating(movie, 8);
    Rating rating3 = new Rating(movie, 7);
    Rating rating4 = new Rating(movie, 8);
    Rating rating5 = new Rating(movie, 6);

    Rating rating6 = new Rating(movie2, 9);
    Rating rating7 = new Rating(movie2, 8);
    Rating rating8 = new Rating(movie2, 7);
    Rating rating9 = new Rating(movie2, 8);
    Rating rating10 = new Rating(movie2, 6);
    Rating rating11 = null;

    List<Rating> ratings = new ArrayList();
    ratings.add(rating1);
    ratings.add(rating2);
    ratings.add(rating3);
    ratings.add(rating4);
    ratings.add(rating5);

    List<Rating> ratingsWithNull = new ArrayList();
    ratingsWithNull.add(rating6);
    ratingsWithNull.add(rating7);
    ratingsWithNull.add(rating8);
    ratingsWithNull.add(rating9);
    ratingsWithNull.add(rating10);
    ratingsWithNull.add(rating11);

    /* Mocker la méthode du RatingRepository pour retourner tous les ratings d'un movie */
  
    Mockito.when(ratingRepository.findAllByMovieId(1))
      .thenReturn(ratings);

    Mockito.when(ratingRepository.findAllByMovieId(2))
      .thenReturn(ratingsWithNull);
  }

  /* Ajouter une méthode de test du calcul des notes */

  @Test
  public void testGoodRatingCalculationForAMovie() throws EmptyRatingException {
    double globalRating = ratingService.getRatingForMovie(1);

    assertThat(globalRating).isEqualTo(7.6);
  }

  @Test(expected = EmptyRatingException.class)
  public void testWithNullRatingThrowsException() throws EmptyRatingException {
    double globalRating = ratingService.getRatingForMovie(2);
  }
}