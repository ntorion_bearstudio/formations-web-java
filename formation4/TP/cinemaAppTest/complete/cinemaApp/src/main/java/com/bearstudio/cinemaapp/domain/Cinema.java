package com.bearstudio.cinemaapp.domain;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.*;

@Entity
public class Cinema implements Serializable {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id;

    @Column(name="name")
    private String name;

    @Column(name="city")
    private String city;

    public Cinema() {}

    public Cinema(String name, String city) {
      this.name = name;
      this.city = city;
    }

    public Cinema(int id, String name, String city) {
        this.id = id;
        this.name = name;
        this.city = city;
    }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }
    
    public int getId() { return id; }

    public void setId(int id) { this.id = id; }

    public String getCity() { return city; }

    public void setCity(String city) { this.city = city; }
}