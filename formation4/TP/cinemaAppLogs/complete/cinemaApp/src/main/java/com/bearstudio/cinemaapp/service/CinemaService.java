package com.bearstudio.cinemaapp.service;

import java.util.*;

import com.bearstudio.cinemaapp.domain.*;
import com.bearstudio.cinemaapp.repository.CinemaRepository;

import org.springframework.stereotype.Service;

@Service
public class CinemaService {

    private final CinemaRepository cinemaRepository;

    public CinemaService(CinemaRepository cinemaRepository) {
        this.cinemaRepository = cinemaRepository;
    }

    public void createCinema(Cinema cinema) {
        this.cinemaRepository.save(cinema);
    }

    public void updateCinema(Cinema cinema) {
        this.cinemaRepository.save(cinema);
    }

    public List<Cinema> getCinemas() {
        return this.cinemaRepository.findAll();
    }

    public Optional<Cinema> getCinema(int id) {
        return this.cinemaRepository.findById(id);
    }

    public void deleteCinema(int id) {
        this.cinemaRepository.deleteById(id);
    }
}