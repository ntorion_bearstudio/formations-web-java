package com.bearstudio.cinemaapp.service;

import java.util.*;

import com.bearstudio.cinemaapp.domain.*;
import com.bearstudio.cinemaapp.repository.MovieRepository;

import org.springframework.stereotype.Service;

@Service
public class MovieService {

    private final MovieRepository movieRepository;

    public MovieService(MovieRepository movieRepository) {
        this.movieRepository = movieRepository;
    }

    public void createMovie(Movie movie) {
        this.movieRepository.save(movie);
    }

    public void updateMovie(Movie movie) {
        this.movieRepository.save(movie);
    }

    public List<Movie> getMovies() {
        return this.movieRepository.findAll();
    }

    public Optional<Movie> getMovie(int id) {
        return this.movieRepository.findById(id);
    }

    public void deleteMovie(int id) {
        this.movieRepository.deleteById(id);
    }

    public List<Movie> getMoviesForCity(String city) {
        return this.movieRepository.findAllByCinemaCity(city);
    }
}