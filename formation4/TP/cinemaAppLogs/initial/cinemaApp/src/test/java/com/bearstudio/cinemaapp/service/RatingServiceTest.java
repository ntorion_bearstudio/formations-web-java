package test.java.com.bearstudio.cinemaapp.service;

import java.util.*;

import org.springframework.web.bind.annotation.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import javax.persistence.EntityManager;

import com.bearstudio.cinemaapp.CinemaApplication;
import com.bearstudio.cinemaapp.domain.Cinema;
import com.bearstudio.cinemaapp.domain.Movie;
import com.bearstudio.cinemaapp.domain.Rating;
import com.bearstudio.cinemaapp.repository.RatingRepository;
import com.bearstudio.cinemaapp.service.RatingService;

import static org.assertj.core.api.Assertions.assertThat;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.mockito.Mockito;

import java.time.LocalDate;

/**
 * Test class for the RatingService.
 *
 * @see RatingService
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = CinemaApplication.class)
public class RatingServiceTest {

  @MockBean
  private RatingRepository ratingRepository;

  @Autowired
  private RatingService ratingService;

  @Autowired
  private EntityManager entityManager;

  private List<Rating> ratings;

  @Before
  public void setup() {
    MockitoAnnotations.initMocks(this);
  }

  @Before
  public void initTests() {
    List<Rating> ratings = new ArrayList<>();

    Cinema cinema = new Cinema(1, "Gaumont", "Paris");

    Movie movie = new Movie(1, "Titanic", LocalDate.parse("1997-10-10"), "synopsis", cinema);

    Rating rating1 = new Rating();
    rating1.setMovie(movie);
    rating1.setRating(9);

    Rating rating2 = new Rating();
    rating2.setMovie(movie);
    rating2.setRating(7);

    Rating rating3 = new Rating();
    rating3.setMovie(movie);
    rating3.setRating(8);

    Rating rating4 = new Rating();
    rating4.setMovie(movie);
    rating4.setRating(6);

    ratings.add(rating1);
    ratings.add(rating2);
    ratings.add(rating3);
    ratings.add(rating4);

    Mockito.when(ratingRepository.findAllByMovieId(1))
      .thenReturn(ratings);
  }

  @Test
  public void testRatingCalculation() {
    // when
    double rating = ratingService.getRatingForMovie(1);

    // then
    assertThat(rating)
      .isEqualTo(7.5);
  }
}