package com.bearstudio.cinemaapp.service;

import java.util.*;
import java.util.stream.*;

import com.bearstudio.cinemaapp.domain.*;
import com.bearstudio.cinemaapp.repository.*;

import org.springframework.stereotype.Service;
import org.slf4j.*;

@Service
public class RatingService {

    private final RatingRepository ratingRepository;

    private Logger logger;

    public RatingService(RatingRepository ratingRepository) {
        this.ratingRepository = ratingRepository;
        this.logger = LoggerFactory.getLogger(RatingService.class);
    }

    public void addRating(Rating rating) {
        this.ratingRepository.save(rating);
    }

    public double getRatingForMovie(int movieId) {
        logger.debug("Calculating global rating for movie id : " + movieId);

        List<Rating> allRatingsForMovie = this.ratingRepository.findAllByMovieId(movieId);
        double rating = 0.0;
        rating += allRatingsForMovie.stream().mapToInt(r -> {
          logger.warn("Rating : " + r.getRating());
          if (r.getRating() == null) {
            logger.warn("One rating is null for movie id : " + movieId);
          }
          return r.getRating();
        }).sum();
        rating /= allRatingsForMovie.size();
        return rating;
    }
}