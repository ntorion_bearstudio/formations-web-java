package com.bearstudio.cinemaapp.domain;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.*;

@Entity
public class Rating implements Serializable {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    private Movie movie;

    @Column(name="rating")
    private Integer rating;

    public Rating() {}

    public Rating(Movie movie, Integer rating) {
        this.movie = movie;
        this.rating = rating;
    }

    public Movie getMovie() { return movie; }

    public void setMovie(Movie movie) { this.movie = movie; }

    public Integer getRating() { return rating; }

    public void setRating(Integer rating) { this.rating = rating; }
}