package com.bearstudio.cinemaapp.web.rest;

import java.util.*;

import com.bearstudio.cinemaapp.domain.*;
import com.bearstudio.cinemaapp.service.CinemaService;

import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class CinemaResource {

    private final CinemaService cinemaService;

    public CinemaResource(CinemaService cinemaService) {
        this.cinemaService = cinemaService;
    }

    @GetMapping("/cinemas")
    public List<Cinema> getCinemas() {
        return cinemaService.getCinemas();
    }

    @GetMapping("/cinema/{id}")
    public Cinema getCinema(@PathVariable int id) {
        Optional<Cinema> cinema = cinemaService.getCinema(id);
        if (cinema.isPresent()) {
            return cinema.get();
        }
        return null;
    }

    @DeleteMapping("/cinema/{id}")
    public void deleteCinema(@PathVariable int id) {
        cinemaService.deleteCinema(id);
    }

    @PostMapping("/cinema")
    public void createCinema(@RequestBody Cinema cinema) {
        cinemaService.createCinema(cinema);
    }

    @PutMapping("/cinema")
    public void updateCinema(@RequestBody Cinema cinema) {
        cinemaService.updateCinema(cinema);
    }
}