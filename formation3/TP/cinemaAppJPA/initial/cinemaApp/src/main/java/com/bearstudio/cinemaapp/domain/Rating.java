package com.bearstudio.cinemaapp.domain;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.*;

@Entity
public class Rating implements Serializable {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    private Movie movie;

    @Column(name="rating")
    private int rating;

    public Rating() {}

    public Rating(Movie movie, int rating) {
        this.movie = movie;
        this.rating = rating;
    }

    public Movie getMovie() { return movie; }

    public void setMovie(Movie movie) { this.movie = movie; }

    public int getRating() { return rating; }

    public void setRating(int rating) { this.rating = rating; }
}