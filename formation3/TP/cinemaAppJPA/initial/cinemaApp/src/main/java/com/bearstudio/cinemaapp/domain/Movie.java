package com.bearstudio.cinemaapp.domain;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.*;

@Entity
public class Movie implements Serializable {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id;

    @Column(name="name")
    private String name;

    @Column(name="synopsis")
    private String synopsis;

    @Column(name="release_date")
    private LocalDate releaseDate;

    public Movie() {}

    public Movie(int id, String name, LocalDate releaseDate, String synopsis) {
        this.id = id;
        this.name = name;
        this.synopsis = synopsis;
        this.releaseDate = releaseDate;
    }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public String getSynopsis() { return synopsis; }

    public void setSynopsis(String synopsis) { this.synopsis = synopsis; }

    public LocalDate getReleaseDate() { return releaseDate; }

    public void setReleaseDate(LocalDate releaseDate) { this.releaseDate = releaseDate; }

    public int getId() { return id; }

    public void setId(int id) { this.id = id; }
}