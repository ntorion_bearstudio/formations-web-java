package com.bearstudio.cinemaapp.repository;

import java.util.ArrayList;
import java.util.List;

import com.bearstudio.cinemaapp.domain.*;

import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.stereotype.Repository;

@Repository
public interface MovieRepository extends JpaRepository<Movie, Integer> {
    List<Movie> findAllByCinemaCity(String city);
}