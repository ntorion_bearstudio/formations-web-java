package com.bearstudio.cinemaapp.service;

import java.util.*;
import java.util.stream.*;

import com.bearstudio.cinemaapp.domain.*;
import com.bearstudio.cinemaapp.repository.*;

import org.springframework.stereotype.Service;

@Service
public class RatingService {

    private final RatingRepository ratingRepository;

    public RatingService(RatingRepository ratingRepository) {
        this.ratingRepository = ratingRepository;
    }

    public void addRating(Rating rating) {
        this.ratingRepository.save(rating);
    }

    public double getRatingForMovie(int movieId) {
        List<Rating> allRatingsForMovie = this.ratingRepository.findAllByMovieId(movieId);
        double rating = 0.0;
        rating += allRatingsForMovie.stream().mapToInt(r -> r.getRating()).sum();
        rating /= allRatingsForMovie.size();
        return rating;
    }
}