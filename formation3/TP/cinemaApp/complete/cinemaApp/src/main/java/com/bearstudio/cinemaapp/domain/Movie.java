package com.bearstudio.cinemaapp.domain;

import java.io.Serializable;
import java.time.LocalDate;

public class Movie implements Serializable {

    private int id;
    private String name;
    private String synopsis;
    private LocalDate releaseDate;

    public Movie() {}

    public Movie(int id, String name, LocalDate releaseDate, String synopsis) {
        this.id = id;
        this.name = name;
        this.synopsis = synopsis;
        this.releaseDate = releaseDate;
    }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public String getSynopsis() { return synopsis; }

    public void setSynopsis(String synopsis) { this.synopsis = synopsis; }

    public LocalDate getReleaseDate() { return releaseDate; }

    public void setReleaseDate(LocalDate releaseDate) { this.releaseDate = releaseDate; }

    public int getId() { return id; }

    public void setId(int id) { this.id = id; }
}