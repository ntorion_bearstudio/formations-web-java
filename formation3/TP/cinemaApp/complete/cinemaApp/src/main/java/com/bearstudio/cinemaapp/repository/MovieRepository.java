package com.bearstudio.cinemaapp.repository;

import java.util.ArrayList;
import java.util.List;

import com.bearstudio.cinemaapp.domain.*;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import org.springframework.stereotype.Repository;

@Repository
public class MovieRepository {

    private NamedParameterJdbcTemplate template;

    public MovieRepository(NamedParameterJdbcTemplate template) {
        this.template = template;
    }

    public void createMovie(Movie movie) {
        String sql = "insert into movie (name, synopsis, release_date) values (:name, :synopsis, :release_date)";
        KeyHolder holder = new GeneratedKeyHolder();
        SqlParameterSource param = new MapSqlParameterSource()
            .addValue("name", movie.getName())
            .addValue("synopsis", movie.getSynopsis())
            .addValue("release_date", movie.getReleaseDate());
        template.update(sql, param, holder);
    }

    public void updateMovie(Movie movie) {
        String sql = "update movie set name=:name, synopsis=:synopsis, release_date=:release_date where id=:id";
        KeyHolder holder = new GeneratedKeyHolder();
        SqlParameterSource param = new MapSqlParameterSource()
            .addValue("id", movie.getId())
            .addValue("name", movie.getName())
            .addValue("synopsis", movie.getSynopsis())
            .addValue("release_date", movie.getReleaseDate());
        template.update(sql, param, holder);
    }

    public Movie getMovie(int id) {
        List<Movie> movies = template.query("select * from movie where id=" + id, new MovieRowMapper());
        if (movies.size() > 0) {
            return movies.get(0);            
        }
        return null;
    }

    public void deleteMovie(int id) {
        String sql = "delete from movie where id=:id";
        KeyHolder holder = new GeneratedKeyHolder();
        SqlParameterSource param = new MapSqlParameterSource()
            .addValue("id", id);
        template.update(sql, param, holder);
    }

    public List<Movie> getMovies() {
        return template.query("select * from movie", new MovieRowMapper());
    }
}