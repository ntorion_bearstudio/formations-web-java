package com.bearstudio.cinemaapp.repository;

import org.springframework.jdbc.core.RowMapper;
import com.bearstudio.cinemaapp.domain.Rating;

import java.sql.ResultSet;
import java.sql.SQLException;

public class RatingRowMapper implements RowMapper<Rating> {
    @Override
    public Rating mapRow(ResultSet rs, int arg1) throws SQLException {
        Rating rating = new Rating();
        rating.setMovieId(Integer.parseInt(rs.getString("movie_id")));
        rating.setRating(Integer.parseInt(rs.getString("rating")));
        return rating;
    }
}