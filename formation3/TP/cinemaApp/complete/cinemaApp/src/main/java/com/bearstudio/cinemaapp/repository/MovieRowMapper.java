package com.bearstudio.cinemaapp.repository;

import org.springframework.jdbc.core.RowMapper;
import com.bearstudio.cinemaapp.domain.Movie;

import java.time.LocalDate;
import java.sql.ResultSet;
import java.sql.SQLException;

public class MovieRowMapper implements RowMapper<Movie> {
    @Override
    public Movie mapRow(ResultSet rs, int arg1) throws SQLException {
        Movie movie = new Movie();
        movie.setId(Integer.parseInt(rs.getString("id")));
        movie.setName(rs.getString("name"));
        movie.setSynopsis(rs.getString("synopsis"));
        movie.setReleaseDate(LocalDate.parse(rs.getString("release_date")));
        return movie;
    }
}