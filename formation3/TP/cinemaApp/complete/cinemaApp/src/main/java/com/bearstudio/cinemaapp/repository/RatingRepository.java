package com.bearstudio.cinemaapp.repository;

import java.util.ArrayList;
import java.util.List;

import com.bearstudio.cinemaapp.domain.*;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import org.springframework.stereotype.Repository;

@Repository
public class RatingRepository {

    private NamedParameterJdbcTemplate template;

    public RatingRepository(NamedParameterJdbcTemplate template) {
        this.template = template;
    }

    public void createRating(Rating rating) {
        String sql = "insert into rating (movie_id, rating) values (:movie_id, :rating)";
        KeyHolder holder = new GeneratedKeyHolder();
        SqlParameterSource param = new MapSqlParameterSource()
            .addValue("movie_id", rating.getMovieId())
            .addValue("rating", rating.getRating());
        template.update(sql, param, holder);
    }

    public List<Rating> getRatings() {
        return template.query("select * from rating", new RatingRowMapper());
    }
}