package com.bearstudio.cinemaapp.service;

import java.util.*;
import java.util.stream.*;

import com.bearstudio.cinemaapp.domain.*;
import com.bearstudio.cinemaapp.repository.*;

import org.springframework.stereotype.Service;

@Service
public class RatingService {

    private final RatingRepository ratingRepository;

    public RatingService(RatingRepository ratingRepository) {
        this.ratingRepository = ratingRepository;
    }

    public void addRating(Rating rating) {
        this.ratingRepository.createRating(rating);
    }

    public double getRatingForMovie(int movieId) {
        List<Rating> allRatings = this.ratingRepository.getRatings();
        List<Rating> allMovieRatings = allRatings.stream()
                                        .filter(rating -> rating.getMovieId() == movieId)
                                        .collect(Collectors.toList());  
        double rating = 0.0;
        rating += allMovieRatings.stream().mapToInt(r -> r.getRating()).sum();
        rating /= allMovieRatings.size();
        return rating;
    }
}