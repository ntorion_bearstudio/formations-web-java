package com.bearstudio.cinemaapp.web.rest;

import java.util.List;

import com.bearstudio.cinemaapp.domain.*;
import com.bearstudio.cinemaapp.service.RatingService;

import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class RatingResource {

    private final RatingService ratingService;

    public RatingResource(RatingService ratingService) {
        this.ratingService = ratingService;
    }

    @GetMapping("/rating/{movieId}")
    public double getMovieRating(@PathVariable("movieId") int movieId) {
        return ratingService.getRatingForMovie(movieId);
    }

    @PostMapping("/rating")
    public void addRatingForMovie(@RequestBody Rating rating) {
        ratingService.addRating(rating);
    }
}