package com.bearstudio.cinemaapp.repository;

import java.util.ArrayList;
import java.util.List;

import com.bearstudio.cinemaapp.domain.*;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import org.springframework.stereotype.Repository;

@Repository
public class MovieRepository {

    private NamedParameterJdbcTemplate template;

    public MovieRepository(NamedParameterJdbcTemplate template) {
        this.template = template;
    }

    public void createMovie(Movie movie) {
        /* Ajouter la requête SQL et passer les paramètres sur MapSqlParameterSource */
        String sql = "";
        KeyHolder holder = new GeneratedKeyHolder();
        SqlParameterSource param = new MapSqlParameterSource();
        template.update(sql, param, holder);
    }

    public void updateMovie(Movie movie) {
        /* Ajouter la requête SQL et passer les paramètres sur MapSqlParameterSource */
        String sql = "";
        KeyHolder holder = new GeneratedKeyHolder();
        SqlParameterSource param = new MapSqlParameterSource();
        template.update(sql, param, holder);
    }

    public Movie getMovie(int id) {
        /* Ajouter la requête SQL */
        return null;
    }

    public void deleteMovie(int id) {
        /* Ajouter la requête SQL et passer les paramètres sur MapSqlParameterSource */
        String sql = "";
        KeyHolder holder = new GeneratedKeyHolder();
        SqlParameterSource param = new MapSqlParameterSource();
        template.update(sql, param, holder);
    }

    public List<Movie> getMovies() {
        /* Ajouter la requête SQL */
        return null;
    }
}