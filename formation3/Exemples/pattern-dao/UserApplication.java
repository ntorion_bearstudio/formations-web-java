public class UserApplication {
 
    private static UserDao userDao;
 
    public static void main(String[] args) {
        userDao = new UserDao();
         
        User user1 = userDao.get(0);
        System.out.println(user1);
        user1.setEmail("nicolas.torion+2@bearstudio.fr");
        userDao.update(user1);

        userDao.save(new User("Rudy", "Baer", "rudy@bearstudio.fr"));
         
        userDao.getAll().forEach(user -> {
            User userItem = (User) user;
            System.out.println(userItem);
        });
    }
}