import java.util.ArrayList;
import java.util.List;
import java.lang.*;

public class UserDao implements Dao<User> {
     
    private List<User> users = new ArrayList<>();
     
    public UserDao() {
        users.add(new User("Nicolas", "Torion", "nicolas.torion@bearstudio.fr"));
        users.add(new User("Renan", "Decamps", "renan.decamps@bearstudio.fr"));
    }
    
    @Override
    public User get(long id) {
        return users.get((int) id);
    }
     
    @Override
    public List<User> getAll() {
        return users;
    }
     
    @Override
    public void save(User user) {
        users.add(user);
    }
     
    @Override
    public void update(User user) {
        users.set(users.indexOf(user), user);
    }
     
    @Override
    public void delete(User user) {
        users.remove(user);
    }
}
