package com.bearstudio.gamesmarket.web.rest;

import java.util.*;

import com.bearstudio.gamesmarket.domain.*;
import com.bearstudio.gamesmarket.service.EditorService;

import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class EditorResource {

    private final EditorService editorService;

    public EditorResource(EditorService editorService) {
        this.editorService = editorService;
    }

    @PostMapping("/editors/create")
    public void createEditor(@RequestBody Editor editor) {
        editorService.saveEditor(editor);
    }
}