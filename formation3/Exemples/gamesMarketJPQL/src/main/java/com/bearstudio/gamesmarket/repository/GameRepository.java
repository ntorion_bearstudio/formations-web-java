package com.bearstudio.gamesmarket.repository;

import java.util.ArrayList;
import java.util.List;

import com.bearstudio.gamesmarket.domain.Game;

import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface GameRepository extends JpaRepository<Game, Integer> {
    List<Game> findAllByEditorName(String editorName);
}