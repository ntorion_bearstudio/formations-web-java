package com.bearstudio.gamesmarket.service;

import java.util.*;

import com.bearstudio.gamesmarket.domain.*;
import com.bearstudio.gamesmarket.repository.GameRepository;

import org.springframework.stereotype.Service;

@Service
public class GameService {

    private final GameRepository gameRepository;

    public GameService(GameRepository gameRepository) {
        this.gameRepository = gameRepository;
    }

    public void saveGame(Game game) {
        this.gameRepository.save(game);
    }

    public void deleteGame(int gameId) {
        this.gameRepository.deleteById(gameId);
    }

    public Optional<Game> getGame(int gameId) {
        return this.gameRepository.findById(gameId);
    }

    public List<Game> getGames() {
        return this.gameRepository.findAll();
    }

    public List<Game> getGamesByEditorName(String editorName) {
        return this.gameRepository.findAllByEditorName(editorName);
    }
}