package com.bearstudio.gamesmarket.repository;

import java.util.ArrayList;
import java.util.List;

import com.bearstudio.gamesmarket.domain.Editor;

import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface EditorRepository extends JpaRepository<Editor, Integer> {
}