package com.bearstudio.gamesmarket.web.rest;

import java.util.List;

import com.bearstudio.gamesmarket.domain.*;
import com.bearstudio.gamesmarket.service.GameService;

import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class GameResource {

    private final GameService gameService;

    public GameResource(GameService gameService) {
        this.gameService = gameService;
    }

    @PostMapping("/games/create")
    public void createGame(@RequestBody Game game) {
        gameService.createGame(game);
    }

    @PutMapping("/games/update")
    public void updateGame(@RequestBody Game game) {
        gameService.updateGame(game);
    }

    @GetMapping("/games/get")
    public List<Game> getGames() {
        return gameService.getGames();
    }

    @GetMapping("/games/get/{id}")
    public Game getGame(@PathVariable("id") int id) {
        return gameService.getGame(id);
    }

    @DeleteMapping("/games/delete/{id}")
    public void deleteGame(@PathVariable("id") int id) {
        gameService.deleteGame(id);
    }
}