package com.bearstudio.gamesmarket.repository;

import java.util.ArrayList;
import java.util.List;

import com.bearstudio.gamesmarket.domain.Game;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import org.springframework.stereotype.Repository;

@Repository
public class GameRepository {

    private NamedParameterJdbcTemplate template;

    public GameRepository(NamedParameterJdbcTemplate template) {
        this.template = template;
    }

    public void createGame(Game game) {
        String sql = "insert into game (name, price) values (:name, :price)";
        KeyHolder holder = new GeneratedKeyHolder();
        SqlParameterSource param = new MapSqlParameterSource()
            .addValue("name", game.getName())
            .addValue("price", game.getPrice());
        template.update(sql, param, holder);
    }

    public void updateGame(Game game) {
        String sql = "update game set name=:name, price=:price where id=:id";
        KeyHolder holder = new GeneratedKeyHolder();
        SqlParameterSource param = new MapSqlParameterSource()
            .addValue("id", game.getId())
            .addValue("name", game.getName())
            .addValue("price", game.getPrice());
        template.update(sql, param, holder);
    }

    public void deleteGame(int gameId) {
        String sql = "delete from game where id=:id";
        KeyHolder holder = new GeneratedKeyHolder();
        SqlParameterSource param = new MapSqlParameterSource()
            .addValue("id", gameId);
        template.update(sql, param, holder);
    }

    public Game getGame(int gameId) {
        List<Game> games = template.query("select * from game where id=" + gameId, new GameRowMapper());
        if (games.size() > 0) {
            return games.get(0);            
        }
        return null;
    }

    public List<Game> getGames() {
        return template.query("select * from game", new GameRowMapper());
    }
}