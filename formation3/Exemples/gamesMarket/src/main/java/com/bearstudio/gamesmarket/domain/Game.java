package com.bearstudio.gamesmarket.domain;

import java.io.Serializable;

public class Game implements Serializable{

    private int id;
    private String name;
    private Long price;

    public Game() {}
    
    public Game(int id, String name, Long price) {
        this.id = id;
        this.name = name;
        this.price = price;
    }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public Long getPrice() { return price; }

    public void setPrice(Long price) { this.price = price; }

    public int getId() { return id; }

    public void setId(int id) { this.id = id; }
}