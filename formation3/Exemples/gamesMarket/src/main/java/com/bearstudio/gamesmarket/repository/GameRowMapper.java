package com.bearstudio.gamesmarket.repository;

import org.springframework.jdbc.core.RowMapper;
import com.bearstudio.gamesmarket.domain.Game;

import java.sql.ResultSet;
import java.sql.SQLException;

public class GameRowMapper implements RowMapper<Game> {
    @Override
    public Game mapRow(ResultSet rs, int arg1) throws SQLException {
        Game game = new Game();
        game.setId(Integer.parseInt(rs.getString("id")));
        game.setName(rs.getString("name"));
        game.setPrice(Long.parseLong(rs.getString("price")));
        return game;
    }
}