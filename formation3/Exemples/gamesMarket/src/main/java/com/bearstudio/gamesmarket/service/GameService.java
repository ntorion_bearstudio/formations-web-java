package com.bearstudio.gamesmarket.service;

import java.util.List;

import com.bearstudio.gamesmarket.domain.*;
import com.bearstudio.gamesmarket.repository.GameRepository;

import org.springframework.stereotype.Service;

@Service
public class GameService {

    private final GameRepository gameRepository;

    public GameService(GameRepository gameRepository) {
        this.gameRepository = gameRepository;
    }

    public void createGame(Game game) {
        this.gameRepository.createGame(game);
    }

    public void updateGame(Game game) {
        this.gameRepository.updateGame(game);
    }

    public void deleteGame(int gameId) {
        this.gameRepository.deleteGame(gameId);
    }

    public Game getGame(int gameId) {
        return this.gameRepository.getGame(gameId);
    }

    public List<Game> getGames() {
        return this.gameRepository.getGames();
    }
}