CLASSPATH=$JBOSS_HOME/modules/system/layers/base/javax/servlet/api/main/jboss-servlet-api_4.0_spec-1.0.0.Final.jar

echo $JBOSS_HOME;
echo $CLASSPATH;

javac -cp $CLASSPATH -sourcepath src -d ./WEB-INF/classes src/model/*.java

jar cf Annonces.war WEB-INF *.jsp *.css
cp Annonces.war $JBOSS_HOME/standalone/deployments
