<%@ page contentType="text/html; charset=utf-8" %>
<%@ page language="java" import="model.*" %>
<div>
    <%
    Annonce annonce = (Annonce) request.getAttribute("annonce");
    int annonceIndex = (int) request.getAttribute("annonceIndex");
    if (annonce != null) {
        String date = "";
        if (annonce.getPublishedAt() != null) {
            date = annonce.getPublishedAt().toString();
        }
        %>
            <div class="card m-2" style="width: 18rem;">
                <div class="card-body">
                    <h5 class="card-title"><%=annonce.getTitle() %></h5>
                    <h6 class="card-subtitle mb-2 text-muted"><%=date %></h6>
                    <p class="card-text"><%=annonce.getDescription() %></p>
                    <h5 class="card-title"><%=annonce.getPrice().toString() %>€</h5>
                    <a href="#" class="card-link"><%=annonce.getEmail() %></a>
                    <a href="./index.jsp?annonceIndex=<%=annonceIndex %>" class="btn btn-primary">Ajouter au panier</a>
                </div>
            </div>
        <%
    }
    %>
</div>