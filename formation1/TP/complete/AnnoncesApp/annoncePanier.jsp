<%@ page contentType="text/html; charset=utf-8" %>
<%@ page language="java" import="model.*" %>
<div>
    <%
    Annonce annonce = (Annonce) request.getAttribute("annonce");
    if (annonce != null) {
        String date = "";
        if (annonce.getPublishedAt() != null) {
            date = annonce.getPublishedAt().toString();
        }
        %>
            <div class="card m-2" style="width: 18rem;">
                <div class="card-body">
                    <h5 class="card-title"><%=annonce.getTitle() %></h5>
                    <h6 class="card-subtitle mb-2 text-muted"><%=date %></h6>
                    <p class="card-text"><%=annonce.getDescription() %></p>
                    <h5 class="card-title"><%=annonce.getPrice() %>€</h5>
                    <a href="#" class="card-link"><%=annonce.getEmail() %></a>
                </div>
            </div>
        <%
    }
    %>
</div>