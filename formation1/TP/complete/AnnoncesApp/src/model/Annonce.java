package model;

import java.io.Serializable;
import java.time.LocalDate;

public class Annonce implements Serializable {
    private String title;
    private String description;
    private Long price;
    private String email;
    private LocalDate publishedAt;

    public Annonce() {}

    public String getTitle() { return title; }

    public void setTitle(String title) { this.title = title; }

    public String getDescription() { return description; }

    public void setDescription(String description) { this.description = description; }

    public String getEmail() { return email; }

    public void setEmail(String email) { this.email = email; }

    public Long getPrice() { return price; }
    
    public void setPrice(Long price) { this.price = price; }

    public LocalDate getPublishedAt() { return publishedAt; }

    public void setPublishedAt(LocalDate publishedAt) { this.publishedAt = publishedAt; }
}