package model;

import java.io.*;
import java.time.LocalDate;
import java.util.*;

public class AnnoncesData implements Serializable {
    private ArrayList annonces;

    public AnnoncesData() {
        this.load();
    }

    public ArrayList getAnnonces() { return annonces; }

    public void setAnnonces(ArrayList annonces) { this.annonces = annonces; }

    public void addAnnonce(Annonce annonce) {
        annonce.setPublishedAt(LocalDate.now());
        annonces.add(annonce);
    }

    public void load() {
        try {
            FileInputStream fileInputStream = new FileInputStream("/tmp/annoncesData.txt");
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            annonces = (ArrayList) objectInputStream.readObject();
            objectInputStream.close();
        } catch (Exception e) {
            annonces = new ArrayList<>();
        }
    }

    public void save() {
        try {
            FileOutputStream fileOutputStream = new FileOutputStream("/tmp/annoncesData.txt");
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(annonces);
            objectOutputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}