<%@ page contentType="text/html; charset=utf-8" %>
<%@ page language="java" import="model.*" %>
<%@ page language="java" import="java.util.List" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Site d'annonces</title>
    </head>

    <body>
        <div class="container mt-3">
            <h1>Annonces</h1>

            <div class="mt-3">
                <form action="./index.jsp" method="POST">
                    <div class="input-group mt-3">
                        <input type="text" name="title" class="form-control" placeholder="Titre de l'annonce">
                    </div>
                    <div class="input-group mt-3">
                    </div>

                    <div class="input-group mb-3">
                        <input type="number" name="price" class="form-control" placeholder="Prix de l'annonce">
                        <div class="input-group-append">
                            <span class="input-group-text">€</span>
                        </div>
                    </div>
                    <div class="input-group mt-3">
                        <textarea type="text" name="description" class="form-control" placeholder="Description de l'annonce"></textarea>
                    </div>
                    <div class="input-group mt-3">
                        <input type="email" name="email" class="form-control" placeholder="Adresse email">
                    </div>
                    <div class="input-group mt-3">
                        <input class="btn btn-primary" type="submit" value="Publier l'annonce" />
                    </div>
                </form>
            </div>

            <!-- ICI définir les JavaBeans -->
            <!-- Indice : scope="session" pour le panier -->
            
            <a href="./panier.jsp" class="btn btn-secondary btn-lg active mt-5" role="button" aria-pressed="true">Voir le panier ()</a>
            <div class="mt-2 d-flex flex-wrap">
                <!-- ICI Lister les annonces -->
                <!-- Indice : Boucle For -->
                <!-- Inclure le JSP annonce.jsp -->
            </div>
        </div>
    </body>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="style.css">
</html>