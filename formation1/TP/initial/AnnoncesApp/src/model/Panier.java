package model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Panier implements Serializable {
    private List<Annonce> annonces;

    public Panier() {
        this.annonces = new ArrayList<>();
    }

    public List<Annonce> getAnnonces() { return annonces; }

    public void setAnnonces(List<Annonce> annonces) { this.annonces = annonces; }

    public void addAnnonce(Annonce annonce) {
        this.annonces.add(annonce);
    }
}