<%@ page contentType="text/html; charset=utf-8" %>
<%@ page language="java" import="model.*" %>
<div>
    <div class="card m-2" style="width: 18rem;">
        <div class="card-body">
            <h5 class="card-title"><!-- Titre annonce --></h5>
            <h6 class="card-subtitle mb-2 text-muted"><!-- Date annonce --></h6>
            <p class="card-text"><!-- Description annonce --></p>
            <h5 class="card-title"><!-- Prix annonce -->€</h5>
            <a href="#" class="card-link"><!-- Email annonce --></a>
        </div>
    </div>
</div>