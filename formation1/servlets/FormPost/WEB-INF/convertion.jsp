<%@ page contentType="text/html; charset=utf-8" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Résultat conversion</title>
    </head>

    <body>
        <% String speed = request.getParameter("speed"); %>
        <% String speedConverted = request.getAttribute("speedInMetersBySeconds").toString(); %>
        <h1>Une vitesse de <%= speed %> km/h équivaut à <%= speedConverted %> m/s.</h1>
    </body>
</html>