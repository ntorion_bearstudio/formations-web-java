import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.Locale;

public class FormPost extends HttpServlet {
  public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    String speedParam = request.getParameter("speed");
    Long speed = Long.parseLong(speedParam);
    double speedInMetersBySeconds = 0L;
    if (speed != null) {
      speedInMetersBySeconds = speed / 3.6;
    }
    request.setAttribute("speedInMetersBySeconds",  Math.round(speedInMetersBySeconds * 10) / 10.0);

    this.getServletContext().getRequestDispatcher("/WEB-INF/convertion.jsp").forward(request, response);
  }
}