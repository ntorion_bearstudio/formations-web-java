<%@ page contentType="text/html; charset=utf-8" %>
<%@ page language="java" import="model.*" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Inscription</title>
    </head>

    <body>
        <h1>Inscription</h1>
        <form action="index.jsp" method="POST">
            <label>Prénom : </label><input type="text" name="firstName" /><br><br>
            <label>Nom : </label><input type="text" name="lastName" /><br><br>
            <label>Email : </label><input type="text" name="email" /><br><br>
            <label>Age : </label><input type="number" name="age" min="5" max="100" />

            <br /><br />

            <input type="submit" value="S'inscrire" />
        </form>

        <jsp:useBean id="user" class="model.User" scope="page">
            <jsp:setProperty name="user" property="*" />
        </jsp:useBean>

        <jsp:useBean id="users" class="model.UsersTable" scope="page" />

        <%
            if (user != null && user.getFirstName() != null && user.getLastName() != null) {
                users.addUser(user);
                users.save();
            }
        %>

        <br />
        <h2>Utilisateurs existants</h2>
        <br />

        <%
            if (users != null && users.getUsers() != null) {
                %>
                <table border="1">
                    <thead>
                        <th>Prénom</th>
                        <th>Nom</th>
                        <th>Email</th>
                        <th>Age</th>
                    </thead>
                    <tbody>
                        <%
                        for (Object userInTableObject : users.getUsers()) {
                            User userInTable = (User) userInTableObject;
                            %>
                            <tr>
                                <td><%=userInTable.getFirstName() %></td>
                                <td><%=userInTable.getLastName() %></td>
                                <td><%=userInTable.getEmail() %></td>
                                <td><%=userInTable.getAge() %></td>
                            </tr>
                            <%
                        }
                        %>
                    </tbody>
                </table>
                <%
            }
        %>
    </body>
</html>