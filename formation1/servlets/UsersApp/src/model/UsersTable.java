package model;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.io.EOFException;
import java.util.*;

public class UsersTable implements Serializable {
    private ArrayList users;

    public UsersTable() {
        this.load();
    }

    public ArrayList getUsers() { return users; }

    public void setUsers(ArrayList users) { this.users = users; }

    public void addUser(User user) {
        users.add(user);
    }

    public void load() {
        try {
            FileInputStream fileInputStream = new FileInputStream("/tmp/users.txt");
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            users = (ArrayList) objectInputStream.readObject();
        } catch (Exception e) {
            users = new ArrayList<>();
        }
    }

    public void save() {
        try {
            FileOutputStream fileOutputStream = new FileOutputStream("/tmp/users.txt");
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(users);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}