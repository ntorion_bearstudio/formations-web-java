import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.Locale;

public class SimpleGet extends HttpServlet {
  public void doGet(HttpServletRequest requete, HttpServletResponse response) throws ServletException, IOException {
    reponses.setContentType("text/html; charset=UTF-8");
    reponses.setLocale(new Locale(Locale.FRENCH.getLanguage(), Locale.FRANCE.getCountry()));
    PrintWriter out = reponses.getWriter();
    requete.setCharacterEncoding("UTF-8");

    String prenom = requete.getParameter("prenom");
    
    out.println("<html>");
    out.println("<body>");
    out.println("<h1>Bonjour " + prenom + " ! </h1>");
    out.println("</body>");
    out.println("</html>");
  }
}
