CLASSPATH=$JBOSS_HOME/modules/system/layers/base/javax/servlet/api/main/jboss-servlet-api_4.0_spec-1.0.0.Final.jar

echo $CLASSPATH;

javac -cp $CLASSPATH -sourcepath src -d ./WEB-INF/classes src/*.java

jar cf SimpleGetJSP.war WEB-INF index.html
cp SimpleGetJSP.war $JBOSS_HOME/standalone/deployments
