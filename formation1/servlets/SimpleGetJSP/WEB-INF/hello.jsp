<%@ page contentType="text/html; charset=utf-8" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Simple Get With JSP</title>
    </head>

    <body>
        <% String prenom = request.getParameter("prenom"); %>
        <h1>Bonjour <%=(prenom != null && prenom.length() > 0) ? prenom : "inconnu" %> avec JSP ! </h1>
    </body>
</html>