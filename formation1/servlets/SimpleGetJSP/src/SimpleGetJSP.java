import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.Locale;

public class SimpleGetJSP extends HttpServlet {
  public void doGet(HttpServletRequest requete, HttpServletResponse reponse) throws ServletException, IOException {
    this.getServletContext().getRequestDispatcher("/WEB-INF/hello.jsp").forward(requete, reponse);
  }
}